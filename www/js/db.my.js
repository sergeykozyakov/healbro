function initMyDb(db) { 
	if (!db) return; 
	
	db.transaction(function(tx) {
		tx.executeSql('SELECT * FROM healbro_my_calendar', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_calendar (`_id` INTEGER PRIMARY KEY , `name` TEXT , descript TEXT , `date_add` DATETIME , `datetime` DATETIME);', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_complaints', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_complaints (`_id` INTEGER PRIMARY KEY , `id_complaint` INTEGER , `date_add` DATETIME, `id_last_node` INTEGER );', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_diseases', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_diseases (`_id` INTEGER PRIMARY KEY , `name` TEXT, `descript` TEXT, `date_add` DATETIME);', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_pharmacy', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_pharmacy (`_id` INTEGER PRIMARY KEY , `name` TEXT , `descript` TEXT, `quantity` TEXT, `date_add` DATETIME, `date_start` DATETIME, `date_end` DATETIME);', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_pharmacy_hours', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_pharmacy_hours (`_id` INTEGER PRIMARY KEY , `id_pharmacy` INTEGER , `time` TIME);', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_healings', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_healings (`_id` INTEGER PRIMARY KEY , `name` TEXT, `descript` TEXT, `date_add` DATETIME);', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_researches', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_researches (`_id` INTEGER PRIMARY KEY , `name` TEXT, `descript` TEXT, `date_add` DATETIME);', [], null, null);
		});

		tx.executeSql('SELECT * FROM healbro_my_settings', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_settings (`_id` INTEGER PRIMARY KEY , `name` TEXT, `value` INTEGER );', [], null, null);
			tx.executeSql('INSERT INTO healbro_my_settings VALUES( 1, "sex", "0" );', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_speccomps', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_speccomps (`_id` INTEGER PRIMARY KEY , `descript` TEXT, `date_add` DATETIME);', [], null, null);
		});
		
		tx.executeSql('SELECT * FROM healbro_my_tests', [], null, 
		function (tx, error) { 
			tx.executeSql('CREATE TABLE healbro_my_tests (`_id` INTEGER PRIMARY KEY , `name` TEXT, `descript` TEXT, `date_add` DATETIME);', [], null, null);
		});
	});
}