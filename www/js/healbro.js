// Константы
const dynamicPageHashMask = 'complaint-node';
const dynamicPageClass = 'my-dynamic-pages';
const apiUrl = 'https://healbro.ru/api.php';

// Runtime-переменные
var doctorTypeId = 0;
var doctorId = 0;
var doctorCardId = 0;
var pharmacyTerm = '';
var pharmacyCardId = 0;
var systemId = 0;
var organId = 0;
var complaintId = 0;
var nodeId = 0;
var sessionId = 0;
var eventId = 0;
var pharmacyId = 0;

var isWarning = false;
var isSilentAjaxAbort = false;

var currentAjax;

var doctorMap;
var pharmacyMap;

//////////////////////////////////////////////////////////////////////////////////////////////

// Подключение БД
var db = openDatabase('healbro', '1.0', 'HealBro', 5 * 1024 * 1024);

//////////////////////////////////////////////////////////////////////////////////////////////

// Действия при первом запуске
$(document).on('pageinit', '#home', function() {
	initDb(db);
	initMyDb(db);
});

//////////////////////////////////////////////////////////////////////////////////////////////

// Действия при показе страницы записи жалобы
$(document).on('pageshow', '#complaint', function() {
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT value FROM healbro_my_settings WHERE name=?', ['sex'],
			function (tx, result) {
				if (result.rows.length == 1) {
					if (result.rows.item(0)['value'] == 0) {
						$.mobile.changePage('#sex', { role: 'dialog' });
					}
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы тела человека
$(document).on('pageshow', '#complaint-show', function() {
	$('#complaint-show-front-tab').trigger('click');
	
	setActualSize('complaint-show-front', 'complaint-show-rear-tab', 2.62);
	setActualSize('complaint-show-rear', 'complaint-show-rear-tab', 2.62);
	
	$('#complaint-show-front').attr('src', '');
	$('#complaint-show-rear').attr('src', '');
	
	$('#my-map-complaint-show-front').html('');
	$('#my-map-complaint-show-rear').html('');
	
	var sex = 1;
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT healbro_my_settings.value FROM healbro_my_settings WHERE healbro_my_settings.name=?', ['sex'],
			function (tx, result) {
				if (result.rows.length == 1) {
					sex = result.rows.item(0)['value'];
					if (sex == 2) {
						$('#complaint-show-front').attr('src', 'img/woman_front.png');
						$('#complaint-show-rear').attr('src', 'img/woman_rear.png');
					}
					else {
						$('#complaint-show-front').attr('src', 'img/man_front.png');
						$('#complaint-show-rear').attr('src', 'img/man_rear.png');
					}
					
					tx.executeSql('SELECT id_region, id_view, coord, descript FROM healbro_imgmap WHERE id_sex=?', [sex],
					function (tx, result) {
						for (var i = 0; i < result.rows.length; i++) {
							var view = result.rows.item(i)['id_view'];
							
							if (view == 2) {
								$('#my-map-complaint-show-rear').append('<area href="#complaint-organ" shape="poly" class="my-map-organs" data-id="' + result.rows.item(i)['id_region'] + '" data-coords="' + result.rows.item(i)['coord'] + '" alt="' + result.rows.item(i)['descript'] + '">');
							}
							else {
								$('#my-map-complaint-show-front').append('<area href="#complaint-organ" shape="poly" class="my-map-organs" data-id="' + result.rows.item(i)['id_region'] + '" data-coords="' + result.rows.item(i)['coord'] + '" alt="' + result.rows.item(i)['descript'] + '">');
							}
						}
						
						setActualArea('complaint-show-front');
						setActualArea('complaint-show-rear');
					});
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы систем организма
$(document).on('pageshow', '#complaint-systems', function() {
	$('#my-lw-systems').html('');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT _id, name FROM healbro_systems WHERE sex=1', [],
			function (tx, result) {
				for (var i = 0; i < result.rows.length; i++) {
					$('#my-lw-systems').append('<li><a href="#complaint-system" class="my-lw-systems" data-id="' + result.rows.item(i)['_id'] + '" style="white-space:normal;">' + result.rows.item(i)['name'] + '</a></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы симптомов по системе организма
$(document).on('pageshow', '#complaint-system', function() {
	$('#my-lw-complaint-system').html('');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT _id, name, node_id FROM healbro_complaints WHERE system_id=?', [systemId],
			function (tx, result) {
				for (var i = 0; i < result.rows.length; i++) {
					$('#my-lw-complaint-system').append('<li><a href="#complaint-save" class="my-lw-complaints" data-element="' + result.rows.item(i)['_id'] + '" data-id="' + result.rows.item(i)['node_id'] + '" data-rel="dialog" style="white-space:normal;">' + result.rows.item(i)['name'] + '</a></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы симптомов по части тела
$(document).on('pageshow', '#complaint-organ', function() {
	$('#my-lw-complaint-organ').html('');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT healbro_complaints._id, healbro_complaints.name, healbro_complaints.node_id FROM healbro_imgmap2compl, healbro_complaints WHERE healbro_imgmap2compl.id_region=? AND healbro_imgmap2compl.id_complaint=healbro_complaints._id', [organId],
			function (tx, result) {
				for (var i = 0; i < result.rows.length; i++) {
					$('#my-lw-complaint-organ').append('<li><a href="#complaint-save" class="my-lw-complaints" data-element="' + result.rows.item(i)['_id'] + '" data-id="' + result.rows.item(i)['node_id'] + '" data-rel="dialog" style="white-space:normal;">' + result.rows.item(i)['name'] + '</a></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе динамической cтраницы вопросов
$(document).on('pageshow', '.' + dynamicPageClass, function() {
	var hash = $(this).attr('id');
	var id = hash.split(dynamicPageHashMask + '-')[1];
		
	$('#my-lw-question-' + id).html('');
	$('#my-lw-answers-' + id).html('');
	$('#my-lw-answers-doctors-' + id).html('');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT question FROM healbro_nodes WHERE _id=?', [id],
			function (tx, result) {
				if (result.rows.length == 1) {
					$('#my-lw-question-' + id).append('<li style="white-space:normal;">' + result.rows.item(0)['question'] + '</li>').listview('refresh');
				}
			});
			
			tx.executeSql('SELECT id_target_node, answer FROM healbro_answers WHERE id_parent_node=?', [id],
			function (tx, result) {
				for (var i = 0; i < result.rows.length; i++) {
					$('#my-lw-answers-' + id).append('<li><a href="#" class="my-lw-answers" data-id="' + result.rows.item(i)['id_target_node'] + '" style="white-space:normal;">' + result.rows.item(i)['answer'] + '</a></li>').listview('refresh');
				}
			});
			
			tx.executeSql('SELECT healbro_node2docs.id_doctor, healbro_spr_doctors.name, healbro_spr_doctors.preparation FROM healbro_node2docs, healbro_spr_doctors WHERE healbro_node2docs.id_doctor=healbro_spr_doctors._id AND healbro_node2docs.id_node=? AND healbro_node2docs.warning=?', [id, isWarning ? 0 : 1],
			function (tx, result) {
				if (result.rows.length > 0) {
					$('#my-lw-answers-doctors-' + id).append('<li data-role="list-divider">Врачи и анализы</li>').listview('refresh');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					$('#my-lw-answers-doctors-' + id).append('<li><a href="#" class="my-lw-doctor" data-id="' + result.rows.item(i)['id_doctor'] + '" style="white-space:normal;">' + result.rows.item(i)['name'] + '</a><a href="#" class="my-lw-doctor-split" data-text="' + result.rows.item(i)['preparation'] + '" data-icon="info">Информация</a></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы моих жалоб
$(document).on('pageshow', '#health-card-complaints', function() {
	$('#my-lw-health-card-complaints').html('');
	$('#my-text-health-card-complaints').html('Список жалоб пуст!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT healbro_complaints.name, DATE(date_add) as date FROM healbro_my_complaints, healbro_complaints WHERE healbro_my_complaints.id_complaint=healbro_complaints._id ORDER BY healbro_my_complaints.date_add DESC LIMIT 100', [],
			function (tx, result) {
				if (result.rows.length > 0)	{
					$('#my-lw-health-card-complaints').append('<li data-role="list-divider">Жалобы из списка симптомов</li>').listview('refresh');
					$('#my-text-health-card-complaints').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['date'].split('-');
					$('#my-lw-health-card-complaints').append('<li><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p class="ui-li-aside"><strong>' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></li>').listview('refresh');
				}
			});
			
			tx.executeSql('SELECT descript, DATE(date_add) as date FROM healbro_my_speccomps ORDER BY healbro_my_speccomps.date_add DESC LIMIT 100', [],
			function (tx, result) {
				if (result.rows.length > 0) {
					$('#my-lw-health-card-complaints').append('<li data-role="list-divider">Жалобы, введенные вручную</li>').listview('refresh');
					$('#my-text-health-card-complaints').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['date'].split('-');
					$('#my-lw-health-card-complaints').append('<li><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['descript'] + '</h2><p class="ui-li-aside"><strong>' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы моих болезней
$(document).on('pageshow', '#health-card-diseases', function() {
	$('#my-lw-health-card-diseases').html('');
	$('#my-text-health-card-diseases').html('Список болезней пуст!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT name, descript, DATE(date_add) as date FROM healbro_my_diseases ORDER BY healbro_my_diseases.date_add DESC LIMIT 200', [],
			function (tx, result) {
				if (result.rows.length > 0) {
					$('#my-text-health-card-diseases').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['date'].split('-');
					$('#my-lw-health-card-diseases').append('<li><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p style="white-space:normal;">' + result.rows.item(i)['descript'] + '</p><p class="ui-li-aside"><strong>' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы моих исследований
$(document).on('pageshow', '#health-card-researches', function() {
	$('#my-lw-health-card-researches').html('');
	$('#my-text-health-card-researches').html('Список исследований пуст!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT name, descript, DATE(date_add) as date FROM healbro_my_researches ORDER BY healbro_my_researches.date_add DESC LIMIT 200', [],
			function (tx, result) {
				if (result.rows.length > 0) {
					$('#my-text-health-card-researches').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['date'].split('-');
					$('#my-lw-health-card-researches').append('<li><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p style="white-space:normal;">' + result.rows.item(i)['descript'] + '</p><p class="ui-li-aside"><strong>' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы моих анализов
$(document).on('pageshow', '#health-card-tests', function() {
	$('#my-lw-health-card-tests').html('');
	$('#my-text-health-card-tests').html('Список анализов пуст!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT name, descript, DATE(date_add) as date FROM healbro_my_tests ORDER BY healbro_my_tests.date_add DESC LIMIT 200', [],
			function (tx, result) {
				if (result.rows.length > 0) {
					$('#my-text-health-card-tests').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['date'].split('-');
					$('#my-lw-health-card-tests').append('<li><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p style="white-space:normal;">' + result.rows.item(i)['descript'] + '</p><p class="ui-li-aside"><strong>' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы моих лечений
$(document).on('pageshow', '#health-card-healings', function() {
	$('#my-lw-health-card-healings').html('');
	$('#my-text-health-card-healings').html('Список лечений пуст!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT name, descript, DATE(date_add) as date FROM healbro_my_healings ORDER BY healbro_my_healings.date_add DESC LIMIT 200', [],
			function (tx, result) {
				if (result.rows.length > 0) {
					$('#my-text-health-card-healings').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['date'].split('-');
					$('#my-lw-health-card-healings').append('<li><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p style="white-space:normal;">' + result.rows.item(i)['descript'] + '</p><p class="ui-li-aside"><strong>' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы моего календаря
$(document).on('pageshow', '#health-calendar', function() {
	$('#my-lw-health-calendar').html('');
	$('#my-text-health-calendar').html('Календарь пуст!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT DATE(datetime) as date, COUNT(*) as cnt FROM healbro_my_calendar GROUP BY DATE(datetime) ORDER BY datetime DESC', [],
			function (tx, result) {
				if (result.rows.length > 0) {
					$('#my-text-health-calendar').html('');
				}
				
				var arrEvents = [];
				
				for (var i = 0; i < result.rows.length; i++) {
					arrEvents[result.rows.item(i)['date']] = { cnt: result.rows.item(i)['cnt'] }
				}
				
				tx.executeSql('SELECT _id, name, descript, datetime FROM healbro_my_calendar ORDER BY datetime DESC LIMIT 5000', [],
				function (tx, result) {
					for (var i = 0; i < result.rows.length; i++) {
						var date = result.rows.item(i)['datetime'].split(' ')[0];
						var time = result.rows.item(i)['datetime'].split(' ')[1].substring(0, 5);
						
						if (arrEvents[date] != undefined) {
							var cnt = arrEvents[date].cnt;
							var arrDate = date.split('-');
							
							$('#my-lw-health-calendar').append('<li data-role="list-divider">' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '<span class="ui-li-count">' + cnt + '</span></li>').listview('refresh');
							delete arrEvents[date];
						}
						
						$('#my-lw-health-calendar').append('<li><a href="#" class="my-lw-health-calendar" data-id="' + result.rows.item(i)['_id'] + '"><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p style="white-space:normal;">' + result.rows.item(i)['descript'] + '</p><p class="ui-li-aside"><strong>' + time + '</strong></p></a></li>').listview('refresh');
					}
				});
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы добавления/редактирования события моего календаря
$(document).on('pageshow', '#health-calendar-record', function() {
	if (eventId == -1) {
		$('.ui-dialog').dialog('close');
	}
	else if (eventId == 0) {
		$('#my-title-calendar-record').html('Новое событие');
		$('#my-btn-calendar-record').val('Добавить').button('refresh');
		$('#my-btn-calendar-delete').parent('.ui-btn').hide();
	}
	else {
		$('#my-title-calendar-record').html('Изменить событие');
		$('#my-btn-calendar-record').val('Изменить').button('refresh');
		$('#my-btn-calendar-delete').parent('.ui-btn').show();
	}
});

// Действия при показе cтраницы моих лекарств
$(document).on('pageshow', '#health-pharmacy', function() {
	$('#my-lw-health-pharmacy').html('');
	$('#my-text-health-pharmacy').html('Список лекарств пуст!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT _id, name, DATE(date_start) as bdate, descript FROM healbro_my_pharmacy WHERE date_end LIKE ? OR date_end>DATETIME(?, ?) ORDER BY date_start DESC LIMIT 100', ['', 'now', 'localtime'],
			function (tx, result) {
				if (result.rows.length > 0)	{
					$('#my-lw-health-pharmacy').append('<li data-role="list-divider">Лекарства, которые я принимаю сейчас</li>').listview('refresh');
					$('#my-text-health-pharmacy').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['bdate'].split('-');
					$('#my-lw-health-pharmacy').append('<li><a href="#" class="my-lw-health-pharmacy" data-id="' + result.rows.item(i)['_id'] + '"><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p style="white-space:normal;"><strong>' + result.rows.item(i)['descript'] + '</strong></p><p class="ui-li-aside"><strong>с ' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></a><a href="#" class="my-lw-health-pharmacy-split" data-id="' + result.rows.item(i)['_id'] + '">Завершить прием</a></li>').listview('refresh');
				}
			});
			
			tx.executeSql('SELECT _id, name, DATE(date_start) as bdate, DATE(date_end) as edate, descript FROM healbro_my_pharmacy WHERE date_end <> ? AND date_start<DATETIME(?, ?) AND date_end<=DATETIME(?, ?) ORDER BY date_start DESC LIMIT 100', ['', 'now', 'localtime', 'now', 'localtime'],
			function (tx, result) {
				if (result.rows.length > 0)	{
					$('#my-lw-health-pharmacy').append('<li data-role="list-divider">Лекарства, которые я принимал</li>').listview('refresh');
					$('#my-text-health-pharmacy').html('');
				}
				
				for (var i = 0; i < result.rows.length; i++) {
					var arrDate = result.rows.item(i)['bdate'].split('-');
					var arrDateEnd = result.rows.item(i)['edate'].split('-');
					$('#my-lw-health-pharmacy').append('<li><a href="#" class="my-lw-health-pharmacy" data-id="' + result.rows.item(i)['_id'] + '"><h2 style="white-space:normal; padding-top:10px;">' + result.rows.item(i)['name'] + '</h2><p style="white-space:normal;"><strong>' + result.rows.item(i)['descript'] + '</strong></p><p style="white-space:normal;">до ' + arrDateEnd[2] + '.' + arrDateEnd[1] + '.' + arrDateEnd[0] + '</p><p class="ui-li-aside"><strong>с ' + arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0] + '</strong></p></a></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы поиска врача, лабораторных исследований, инструментальной диагностики
$(document).on('pageshow', '#doctor', function() {
	$('#my-lw-doctor').html('');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT _id, name FROM healbro_spr_doctors WHERE type=?', [doctorTypeId],
			function (tx, result) {
				for (var i = 0; i < result.rows.length; i++) {
					$('#my-lw-doctor').append('<li><a href="#" class="my-lw-doctor" data-id="' + result.rows.item(i)['_id'] + '" style="white-space:normal;">' + result.rows.item(i)['name'] + '</a></li>').listview('refresh');
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при показе cтраницы результатов поиска врача, лабораторных исследований, инструментальной диагностики
$(document).on('pageshow', '#doctor-results', function() {
	$('#doctor-results-list-tab').trigger('click');
	setActualSize('my-doctor-map', 'doctor-results-map-tab', 0);
});

// Действия при показе cтраницы результатов поиска лекарств и медицинских товаров
$(document).on('pageshow', '#pharmacy-results', function() {
	$('#pharmacy-results-list-tab').trigger('click');
	setActualSize('my-pharmacy-map', 'pharmacy-results-map-tab', 0);
});

// Действия при показе cтраницы интересного факта
$(document).on('pageshow', '#funny', function() {
	$('#my-text-funny').html('Интересных фактов не найдено!');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT COUNT(*) as num FROM healbro_facts', [],
			function (tx, result) {
				if (result.rows.length == 1) {
					var random = Math.floor(Math.random() * result.rows.item(0)['num']) + 1;
					
					tx.executeSql('SELECT fact FROM healbro_facts WHERE _id=?', [random],
					function (tx, result) {
						if (result.rows.length == 1) {
							$('#my-text-funny').html(result.rows.item(0)['fact']);
						}
					});
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

//////////////////////////////////////////////////////////////////////////////////////////////

// Действия при автозаполнении при поиске лекарств
$(document).on('pagecreate', '#pharmacy', function() {
	$('#my-lw-pharmacy-find').on('filterablebeforefilter', function(e, data) {
		var $ul = $(this),
		$input = $(data.input),
		value = $input.val(),
		html = '';
		$ul.html('');
		
		if (value && value.length > 2) {
			$ul.html('<li><div class="ui-loader"><span class="ui-icon ui-icon-loading"></span></div></li>');
			$ul.listview('refresh');
			$.ajax({
				type: 'POST',
				url: apiUrl + '?mode=get_pharmacy_complete_list&callback=?',
				data: 'term=' + $input.val(),
				dataType: 'jsonp',
				timeout: 30000,
			})
			.then(function(response) {
				$.each(response, function(i, item) {
					html += '<li><a href="#" class="my-btn-pharmacy-find" data-term="' + item + '">' + item + '</a></li>';
				});
				
				$ul.html(html);
				$ul.listview('refresh');
				$ul.trigger('updatelayout');
			});
		}
	});
});

//////////////////////////////////////////////////////////////////////////////////////////////

// Действия при нажатии кнопки "Домой"
$(document).on('click', '.ui-icon-home', function() {
	var count = 0;
	var flag = false;
	
	$.each($.mobile.navigate.history.stack, function(i, item) {
		if (item.hash == '#' + $.mobile.activePage.attr('id')) flag = true;
		if (!flag) count++;
	});
	
	if (currentAjax != undefined) {
		isSilentAjaxAbort = true;
		currentAjax.abort();
	}
	
	window.history.go(-count);
});

// Действия при выборе пола
$(document).on('click', '.my-lw-sex', function() {
	var id = $(this).data('id');
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT value FROM healbro_my_settings WHERE name=?', ['sex'],
			function (tx, result) {
				if (result.rows.length == 1) {
					tx.executeSql('UPDATE healbro_my_settings SET value=? WHERE name=?', [id, 'sex'], 
					function (tx, result) {
						$('.ui-dialog').dialog('close');
					});
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при подтверждении ошибки
$(document).on('click', '#my-btn-error', function() {
	$('.ui-dialog').dialog('close');
});

// Действия при выборе типа поиска врача
$(document).on('click', '.my-lw-doctors', function() {
	doctorTypeId = $(this).data('id');
	$('#my-title-doctor').html($(this).html());
});

// Действия при выборе части тела
$(document).on('click', '.my-map-organs', function() {
	organId = $(this).data('id');
	$('#my-text-complaint-organ').html('Выбрано: <b>' + $(this).attr('alt') + '</b>');
});

// Действия при выборе системы организма
$(document).on('click', '.my-lw-systems', function() {
	systemId = $(this).data('id');
	$('#my-text-complaint-system').html('Выбрано: <b>' + $(this).html() + '</b>');
});

// Действия при выборе симптома
$(document).on('click', '.my-lw-complaints', function() {
	nodeId = $(this).data('id');
	complaintId = $(this).data('element');
	
	$('#my-text-complaint-save').html('Выбран симптом: <b>' + $(this).html() + '</b>');
});

// Действия при выборе необходимости сохранения жалобы
$(document).on('click', '.my-lw-complaint-save', function() {
	var id = $(this).data('id');
	isWarning = false;
	
	if (id == 1) {
		sessionId = 1;
		
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_complaints ORDER BY date_add DESC LIMIT 1', [],
				function (tx, result) {
					if (result.rows.length > 0) {
						var lastId = result.rows.item(0)['_id'];
						
						if (lastId < 100000) {
							sessionId = ++lastId;
						}
					}
					
					tx.executeSql('DELETE FROM healbro_my_complaints WHERE _id=?', [sessionId],
					function (tx, result) {
						tx.executeSql('INSERT INTO healbro_my_complaints VALUES( ?, ?, DATETIME(?, ?), ? )', [sessionId, complaintId, 'now', 'localtime', nodeId],
						function (tx, result) {
							cordova.plugins.notification.local.schedule({
								id: sessionId,
								title: 'Как здоровье?',
								text: 'Коснитесь для ответа',
								every: 'day', 
								firstAt: new Date(new Date().getTime() + 6*3600*1000)
							});
							
							navigateDynamicPage(dynamicPageHashMask, nodeId);
						});
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		sessionId = 0;
		navigateDynamicPage(dynamicPageHashMask, nodeId);
	}
});

// Действия при выборе ответа на вопрос
$(document).on('click', '.my-lw-answers', function() {
	var id = $(this).data('id');
	
	if (sessionId > 0) {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_complaints', [],
				function (tx, result) {
					tx.executeSql('UPDATE healbro_my_complaints SET id_last_node=? WHERE _id=?', [id, sessionId], null, null);
					
					navigateDynamicPage(dynamicPageHashMask, id);
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {	
		navigateDynamicPage(dynamicPageHashMask, id);
	}
});

// Действия при выводе информации о докторе или исследовании
$(document).on('click', '.my-lw-doctor-split', function() {
	var info = $(this).data('text');
	if (!info) info = "Нет дополнительной информации.";
	
	$('#my-text-info').html(info);
	$.mobile.changePage('#info', { role: 'dialog' });
});

// Действия при добавлении своей жалобы из карточки
$(document).on('click', '#my-btn-health-card-complaints-add', function() {
	$.mobile.changePage('#complaint-record', { role: 'dialog' });
});

// Действия при добавлении своей болезни из карточки
$(document).on('click', '#my-btn-health-card-diseases-add', function() {
	$.mobile.changePage('#health-card-disease-record', { role: 'dialog' });
});

// Действия при добавлении своего исследования из карточки
$(document).on('click', '#my-btn-health-card-researches-add', function() {
	$.mobile.changePage('#health-card-research-record', { role: 'dialog' });
});

// Действия при добавлении своего анализа из карточки
$(document).on('click', '#my-btn-health-card-tests-add', function() {
	$.mobile.changePage('#health-card-test-record', { role: 'dialog' });
});

// Действия при добавлении своего лечения из карточки
$(document).on('click', '#my-btn-health-card-healings-add', function() {
	$.mobile.changePage('#health-card-healing-record', { role: 'dialog' });
});

// Действия при добавлении/редактировании события календаря
$(document).on('click', '#my-btn-health-calendar-add, .my-lw-health-calendar', function() {
	eventId = ($(this).data('id') != undefined) ? $(this).data('id') : 0;
	
	var d = new Date();
	
	var day = addZero(d.getDate());
	var month = addZero(d.getMonth()+1);
	var year = d.getFullYear();
	
	$('#my-div-calendar-time-record').html(addTimeWidget('Время события:', 'my-input-calendar-hours-record', 'my-input-calendar-minutes-record', null)).trigger('create');	
	$('#my-input-calendar-name-record').val('');
	$('#my-input-calendar-date-record').val(day + '.' + month + '.' + year);
	$('#my-input-calendar-hours-record').val('00').selectmenu('refresh');
	$('#my-input-calendar-minutes-record').val('00').selectmenu('refresh');
	$('#my-input-calendar-comment-record').val('');
	
	if (eventId > 0) {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT name, descript, datetime FROM healbro_my_calendar WHERE _id=?', [eventId],
				function (tx, result) {
					if (result.rows.length == 1) {
						var date = result.rows.item(0)['datetime'].split(' ')[0];
						var time = result.rows.item(0)['datetime'].split(' ')[1].substring(0, 5);
						
						var arrDate = date.split('-');
						var arrTime = time.split(':');
					
						$('#my-input-calendar-name-record').val(result.rows.item(0)['name']);
						$('#my-input-calendar-date-record').val(arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0]);
						$('#my-input-calendar-hours-record').val(arrTime[0]).selectmenu('refresh');
						$('#my-input-calendar-minutes-record').val(arrTime[1]).selectmenu('refresh');
						$('#my-input-calendar-comment-record').val(result.rows.item(0)['descript']);
						
						$.mobile.changePage('#health-calendar-record', { role: 'dialog' });
					}
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		var minutes = addZero(d.getMinutes());
		minutes = minutes.toString().substr(0, 1) + '0';
		
		$('#my-input-calendar-hours-record').val(addZero(d.getHours())).selectmenu('refresh');
		$('#my-input-calendar-minutes-record').val(minutes).selectmenu('refresh');
		
		$.mobile.changePage('#health-calendar-record', { role: 'dialog' });
	}
});

// Действия при выборе моего лекарства на просмотр
$(document).on('click', '.my-lw-health-pharmacy', function() {
	pharmacyId = $(this).data('id');
	
	$('#my-text-health-pharmacy-name-view').html('');
	$('#my-text-health-pharmacy-comment-view').html('');
	$('#my-text-health-pharmacy-bdate-view').html('');
	$('#my-text-health-pharmacy-edate-view').html('');
	$('#my-text-health-pharmacy-quantity-view').html('');
	$('#my-lw-health-pharmacy-hours-view').html('').listview();
	
	if (db) {
		db.transaction(function(tx) {
			tx.executeSql('SELECT name, descript, quantity, DATE(date_start) as bdate, DATE(date_end) as edate FROM healbro_my_pharmacy WHERE _id=?', [pharmacyId],
			function (tx, result) {
				if (result.rows.length == 1) {
					var arrBdate = result.rows.item(0)['bdate'].split('-');
					var arrEdate = [];
					
					if (result.rows.item(0)['edate'] != null) {
						arrEdate = result.rows.item(0)['edate'].split('-');
					}
					
					$('#my-text-health-pharmacy-name-view').html(result.rows.item(0)['name']);
					$('#my-text-health-pharmacy-comment-view').html(result.rows.item(0)['descript']);
					$('#my-text-health-pharmacy-bdate-view').html('c ' + arrBdate[2] + '.' + arrBdate[1] + '.' + arrBdate[0]);
					if (arrEdate.length > 0) $('#my-text-health-pharmacy-edate-view').html('до ' + arrEdate[2] + '.' + arrEdate[1] + '.' + arrEdate[0]);
					$('#my-text-health-pharmacy-quantity-view').html(result.rows.item(0)['quantity']);
					
					tx.executeSql('SELECT time FROM healbro_my_pharmacy_hours WHERE id_pharmacy=? ORDER BY time ASC', [pharmacyId],
					function (tx, result) {
						if (result.rows.length > 0) {
							$('#my-lw-health-pharmacy-hours-view').append('<li data-role="list-divider">Часы приема</li>').listview('refresh');
						}
						
						for (var i = 0; i < result.rows.length; i++) {
							var time = result.rows.item(i)['time'].substring(0, 5);
							$('#my-lw-health-pharmacy-hours-view').append('<li>' + time + '</li>').listview('refresh');
						}
						
						$.mobile.changePage('#health-pharmacy-view');
					});
				}
			});
		});
	}
	else {
		$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
	
});

// Действия при выборе моего лекарства на закрытие
$(document).on('click', '.my-lw-health-pharmacy-split', function() {
	pharmacyId = $(this).data('id');
	
	$.mobile.changePage('#health-pharmacy-save', { role: 'dialog' });
});

// Действия при добавлении моего лекарства
$(document).on('click', '#my-btn-health-pharmacy-add', function() {
	var d = new Date();
	
	var day = addZero(d.getDate());
	var month = addZero(d.getMonth()+1);
	var year = d.getFullYear();
	
	$('#my-div-pharmacy-time-record').html(addTimeWidget('Добавить часы приема:', 'my-input-pharmacy-hours-record', 'my-input-pharmacy-minutes-record', 'my-btn-pharmacy-time-add-record')).trigger('create');	
	$('#my-input-pharmacy-name-record').val('');
	$('#my-input-pharmacy-date-record').val(day + '.' + month + '.' + year);
	$('#my-input-pharmacy-quantity-record').val('1');
	$('#my-lw-health-pharmacy-hours-record').html('').listview();
	$('#my-input-pharmacy-comment-record').val('');
	
	var minutes = addZero(d.getMinutes());
	minutes = minutes.toString().substr(0, 1) + '0';
	
	$('#my-input-pharmacy-hours-record').val(addZero(d.getHours())).selectmenu('refresh');
	$('#my-input-pharmacy-minutes-record').val(minutes).selectmenu('refresh');
	
	$.mobile.changePage('#health-pharmacy-record', { role: 'dialog' });
});

// Действия при нажатии на кнопку открытия интересных фактов
$(document).on('click', '#my-btn-great', function() {
	$.mobile.changePage('#funny');
});

// Действия при обновлении интересных фактов
$(document).on('click', '#my-btn-funny', function() {
	$.mobile.changePage('#funny', { allowSamePageTransition: true });
});

// Действия при сохранении своей жалобы
$(document).on('click', '#my-btn-complaint-record', function() {
	if ($('#my-input-complaint-record').val().trim() != '') {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_speccomps', [],
				function (tx, result) {
					tx.executeSql('INSERT INTO healbro_my_speccomps VALUES( NULL, ?, DATETIME(?, ?) )', [$('#my-input-complaint-record').val(), 'now', 'localtime'], 
					function (tx, result) {
						$('#my-input-complaint-record').val('');
						
						$('.ui-dialog').dialog('close');
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('#my-text-error').html('Заполните поле жалобы!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при сохранении своей болезни
$(document).on('click', '#my-btn-disease-record', function() {
	if ($('#my-input-disease-name-record').val().trim() != '') {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_diseases', [],
				function (tx, result) {
					tx.executeSql('INSERT INTO healbro_my_diseases VALUES( NULL, ?, ?, DATETIME(?, ?) )', [$('#my-input-disease-name-record').val(), $('#my-input-disease-comment-record').val(), 'now', 'localtime'], 
					function (tx, result) {
						$('#my-input-disease-name-record').val('');
						$('#my-input-disease-comment-record').val('');
						
						$('.ui-dialog').dialog('close');
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('#my-text-error').html('Заполните поле наименования болезни!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при сохранении своего исследования
$(document).on('click', '#my-btn-research-record', function() {
	if ($('#my-input-research-name-record').val().trim() != '') {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_researches', [],
				function (tx, result) {
					tx.executeSql('INSERT INTO healbro_my_researches VALUES( NULL, ?, ?, DATETIME(?, ?) )', [$('#my-input-research-name-record').val(), $('#my-input-research-comment-record').val(), 'now', 'localtime'], 
					function (tx, result) {
						$('#my-input-research-name-record').val('');
						$('#my-input-research-comment-record').val('');
						
						$('.ui-dialog').dialog('close');
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('#my-text-error').html('Заполните поле наименования исследования!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при сохранении своего анализа
$(document).on('click', '#my-btn-test-record', function() {
	if ($('#my-input-test-name-record').val().trim() != '') {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_tests', [],
				function (tx, result) {
					tx.executeSql('INSERT INTO healbro_my_tests VALUES( NULL, ?, ?, DATETIME(?, ?) )', [$('#my-input-test-name-record').val(), $('#my-input-test-comment-record').val(), 'now', 'localtime'], 
					function (tx, result) {
						$('#my-input-test-name-record').val('');
						$('#my-input-test-comment-record').val('');
						
						$('.ui-dialog').dialog('close');
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('#my-text-error').html('Заполните поле наименования анализа!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при сохранении своего лечения
$(document).on('click', '#my-btn-healing-record', function() {
	if ($('#my-input-healing-name-record').val().trim() != '') {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_healings', [],
				function (tx, result) {
					tx.executeSql('INSERT INTO healbro_my_healings VALUES( NULL, ?, ?, DATETIME(?, ?) )', [$('#my-input-healing-name-record').val(), $('#my-input-healing-comment-record').val(), 'now', 'localtime'], 
					function (tx, result) {
						$('#my-input-healing-name-record').val('');
						$('#my-input-healing-comment-record').val('');
						
						$('.ui-dialog').dialog('close');
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('#my-text-error').html('Заполните поле наименования лечения!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при изменении события календаря
$(document).on('click', '#my-btn-calendar-record', function() {
	if ($('#my-input-calendar-name-record').val().trim() != '' && $('#my-input-calendar-date-record').val().trim() != '') {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_calendar ORDER BY date_add DESC LIMIT 1', [],
				function (tx, result) {
					var arrDate = $('#my-input-calendar-date-record').val().split('\.');
					
					var date = arrDate[2] + '-' + arrDate[1] + '-' + arrDate[0];
					var time = $('#my-input-calendar-hours-record').val() + ':' + $('#my-input-calendar-minutes-record').val() + ':00';
					
					var dateStart = new Date(arrDate[2], arrDate[1] - 1, arrDate[0]);
					var dateNow = new Date();
					var timeNow = new Date();
					
					dateNow.setHours(0, 0, 0, 0);
					
					if (eventId > 0) {
						tx.executeSql('UPDATE healbro_my_calendar SET name=?, descript=?, datetime=? WHERE _id=?', [$('#my-input-calendar-name-record').val(), $('#my-input-calendar-comment-record').val(), date + ' ' + time, eventId], 
						function (tx, result) {
							cordova.plugins.notification.local.cancel(eventId, null);
							
							if (dateStart.getTime() >= dateNow.getTime()) {
								dateStart.setHours($('#my-input-calendar-hours-record').val(), $('#my-input-calendar-minutes-record').val(), 0, 0);
								
								if (dateStart.getTime() >= timeNow.getTime()) {
									cordova.plugins.notification.local.schedule({
										id: eventId,
										title: $('#my-input-calendar-hours-record').val() + ':' + $('#my-input-calendar-minutes-record').val() + ' ' + $('#my-input-calendar-name-record').val(),
										text: $('#my-input-calendar-comment-record').val(),
										at: dateStart
									});
								}
							}
							
							eventId = 0;
							$('.ui-dialog').dialog('close');
						});
					}
					else {
						var index = 200001;
							
						if (result.rows.length > 0) {
							var lastId = result.rows.item(0)['_id'];
							
							if (lastId < 300000) {
								index = ++lastId;
							}
						}
						
						tx.executeSql('DELETE FROM healbro_my_calendar WHERE _id=?', [index],
						function (tx, result) {
							cordova.plugins.notification.local.cancel(index, null);
							
							tx.executeSql('INSERT INTO healbro_my_calendar VALUES( ?, ?, ?, DATETIME(?, ?), ? )', [index, $('#my-input-calendar-name-record').val(), $('#my-input-calendar-comment-record').val(), 'now', 'localtime', date + ' ' + time],
							function (tx, result) {
								if (dateStart.getTime() >= dateNow.getTime()) {
									dateStart.setHours($('#my-input-calendar-hours-record').val(), $('#my-input-calendar-minutes-record').val(), 0, 0);
									
									if (dateStart.getTime() >= timeNow.getTime()) {
										cordova.plugins.notification.local.schedule({
											id: index,
											title: $('#my-input-calendar-hours-record').val() + ':' + $('#my-input-calendar-minutes-record').val() + ' ' + $('#my-input-calendar-name-record').val(),
											text: $('#my-input-calendar-comment-record').val(),
											at: dateStart
										});
									}
								}
											
								$('.ui-dialog').dialog('close');
							});
						});
					}
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('#my-text-error').html('Заполните наименование события, дату и время!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при удалении события календаря
$(document).on('click', '#my-btn-calendar-delete', function() {
	$.mobile.changePage('#health-calendar-delete', { role: 'dialog' });
});

// Действия при выборе необходимости удаления события календаря
$(document).on('click', '.my-lw-health-calendar-delete', function() {
	var id = $(this).data('id');
	
	if (id == 1) {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_calendar', [],
				function (tx, result) {
					if (eventId > 0) {
						tx.executeSql('DELETE FROM healbro_my_calendar WHERE _id=?', [eventId], 
						function (tx, result) {
							cordova.plugins.notification.local.cancel(eventId, null);
							
							eventId = -1;
							$('.ui-dialog').dialog('close');
						});
					}
					else {
						$('.ui-dialog').dialog('close');
					}
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('.ui-dialog').dialog('close');
	}
});

// Действия при добавлении времени приема лекарства
$(document).on('click', '#my-btn-pharmacy-time-add-record', function() {
	var time = $('#my-input-pharmacy-hours-record').val() + ':' + $('#my-input-pharmacy-minutes-record').val();
	var flag = false;
	
	$('.my-lw-health-pharmacy-hours-record').each(function(i) {
		if (!flag && $(this).html() == time) flag = true;
	});
	
	if (!flag) {
		$('#my-lw-health-pharmacy-hours-record').append('<li><a href="#" class="my-lw-health-pharmacy-hours-record" style="white-space:normal;">' + time + '</a><a href="#" class="my-lw-health-pharmacy-hours-record-split">Удалить</a></li>').listview('refresh');
	}
});

// Действия при удалении времени приема лекарства
$(document).on('click', '.my-lw-health-pharmacy-hours-record-split', function() {
	$(this).parent().remove();
	$('#my-lw-health-pharmacy-hours-record').listview('refresh');
});

// Действия при добавлении моего лекарства
$(document).on('click', '#my-btn-pharmacy-record', function() {
	if ($('#my-input-pharmacy-name-record').val().trim() != '' && $('#my-input-pharmacy-date-record').val().trim() != '' && $('#my-input-pharmacy-quantity-record').val().trim() != '') {
		if ($('#my-lw-health-pharmacy-hours-record').html().length == 0) {
			$('#my-btn-pharmacy-time-add-record').trigger('click');
		}
		
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_pharmacy ORDER BY date_add DESC LIMIT 1', [],
				function (tx, result) {
					var arrDate = $('#my-input-pharmacy-date-record').val().split('\.');
					var dateTime = arrDate[2] + '-' + arrDate[1] + '-' + arrDate[0] + ' 00:00:00';
					var index = 100001;
					
					if (result.rows.length > 0) {
						var lastId = result.rows.item(0)['_id'];
						
						if (lastId < 200000) {
							index = ++lastId;
						}
					}
					
					tx.executeSql('DELETE FROM healbro_my_pharmacy WHERE _id=?', [index],
					function (tx, result) {
						tx.executeSql('DELETE FROM healbro_my_pharmacy_hours WHERE id_pharmacy=?', [index],
						function (tx, result) {
							tx.executeSql('INSERT INTO healbro_my_pharmacy VALUES( ?, ?, ?, ?, DATETIME(?, ?), ?, ? )', [index, $('#my-input-pharmacy-name-record').val(), $('#my-input-pharmacy-comment-record').val(), $('#my-input-pharmacy-quantity-record').val(), 'now', 'localtime', dateTime, ''],
							function (tx, result) {
								var total = $('.my-lw-health-pharmacy-hours-record').length;
								var timeArr	= [];							
								
								$('.my-lw-health-pharmacy-hours-record').each(function(i) {
									timeArr.push($(this).html());
									
									tx.executeSql('INSERT INTO healbro_my_pharmacy_hours VALUES( NULL, ?, ? )', [index, $(this).html() + ':00'],
									function (tx, result) {
										if (i == total-1) {
											timeArr.sort();
											
											var dateStart = new Date(arrDate[2], arrDate[1] - 1, arrDate[0]);
											var dateNow = new Date();
											var timeNow = new Date();
											
											dateNow.setHours(0, 0, 0, 0);
											
											if (dateStart.getTime() < dateNow.getTime()) {
												dateStart = dateNow;
											}
											
											var timePartsArr = timeArr[0].split(':');
											dateStart.setHours(timePartsArr[0], timePartsArr[1], 0, 0);
											
											if (dateStart.getTime() < timeNow.getTime()) {
												dateStart.setDate(dateStart.getDate() + 1);
											}
											
											cordova.plugins.notification.local.schedule({
												id: index,
												title: 'Примите лекарство ' + $('#my-input-pharmacy-name-record').val(),
												text: 'Также принять в ' + timeArr.join(', '),
												every: 'day',
												firstAt: dateStart
											});
													
											$('.ui-dialog').dialog('close');
										}
									});
								});
							});
						});
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('#my-text-error').html('Заполните наименование лекарства, дату начала, количество и часы приема!');
		$.mobile.changePage('#error', { role: 'dialog' });
	}
});

// Действия при выборе необходимости завершения приема лекарства
$(document).on('click', '.my-lw-health-pharmacy-save', function() {
	var id = $(this).data('id');
	
	if (id == 1) {
		if (db) {
			db.transaction(function(tx) {
				tx.executeSql('SELECT _id FROM healbro_my_pharmacy', [],
				function (tx, result) {
					tx.executeSql('SELECT COUNT(*) as cnt FROM healbro_my_pharmacy WHERE _id=? AND date_start<DATETIME(?, ?)', [pharmacyId, 'now', 'localtime'],
					function (tx, result) {
						if (result.rows.length == 1) {
							if (result.rows.item(0)['cnt'] == 1) {
								tx.executeSql('UPDATE healbro_my_pharmacy SET date_end=DATETIME(?, ?) WHERE _id=?', ['now', 'localtime', pharmacyId],
								function (tx, result) {
									cordova.plugins.notification.local.cancel(pharmacyId, null);
									
									$('.ui-dialog').dialog('close');
								});
							}
							else {
								$('#my-text-error').html('Невозможно завершить прием лекарства. Вы ещё не начали принимать данное лекарство!');
								$.mobile.changePage('#error', { role: 'dialog' });
							}
						}
					});
				});
			});
		}
		else {
			$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
			$.mobile.changePage('#error', { role: 'dialog' });
		}
	}
	else {
		$('.ui-dialog').dialog('close');
	}
});

// Действия при поиске доктора/исследования/диагностики
$(document).on('click', '.my-lw-doctor', function() {
	doctorId = $(this).data('id');
	
	$('#my-lw-doctor-results').html('').listview();
	$('#my-a-doctor-results-banner').attr('href', '').hide();
	$('#my-img-doctor-results-banner').attr('src', '').attr('alt', '').hide();
	$('#my-text-doctor-results').html('Ничего не найдено!');
	
	$.mobile.loading('show', {
		text: 'загрузка данных...',
		textVisible: true,
		theme: 'b'
	});
	
	$.ajax({
		type: 'POST',
		url: apiUrl + '?mode=get_banner&callback=?',
		data: 'doctor_id=' + doctorId,
		dataType: 'jsonp',
		timeout: 30000,
		success: function(data) {
			if (data['img'] != undefined && data['img'] != '' && data['link'] != undefined && data['link'] != '') {
				$('#my-a-doctor-results-banner').attr('href', data['link']).show();
				$('#my-img-doctor-results-banner').attr('src', data['img']).attr('alt', data['desc']).show();
			}
		}
	});
	
	currentAjax = $.ajax({
		type: 'POST',
		url: apiUrl + '?mode=get_doctor_list&callback=?',
		data: 'doctor_id=' + doctorId,
		dataType: 'jsonp',
		timeout: 30000,
		success: function(data) {
			currentAjax = undefined;
			isSilentAjaxAbort = false;
			$.mobile.loading('hide');
			
			try {
				if (doctorMap == undefined) {
					$('#my-doctor-map').width(1).height(1);
					
					doctorMap = new ymaps.Map('my-doctor-map', {
						center: [56.83292145, 60.62570133],
						zoom: 11,
						controls: ['zoomControl', 'typeSelector']
					});
				}				
			
				doctorMap.geoObjects.removeAll();
			}
			catch (e) {
				$('#my-doctor-map').html('<p style="padding: 10px 0px 0px 10px;">Не удалось загрузить карту! Перезапустите приложение.</p>');
			}
				
			if (data[0] != undefined && data[0]['card_id'] != undefined && data[0]['card_id'] != '') {
				var myGeoObjects = [];
			
				$.each(data, function(i, item) {
					if (i == 0) $('#my-text-doctor-results').html('');
					$('#my-lw-doctor-results').append('<li><a href="#" class="my-lw-doctor-results" data-id="' + item['card_id'] + '"><img ' + ((item['logo'] != undefined && item['logo'] != '') ? 'src="' + item['logo'] + '"' : 'style="display: none;"') + '><h2 style="white-space:normal; padding-top:10px;">' + item['name'] + '</h2><p style="white-space:normal;"><strong>' + item['organization'] + '</strong></p><p style="white-space:normal;">' + item['title'] + '</p><p class="ui-li-aside"><strong>' + ((item['price'] != undefined && item['price'] != '') ? item['price'] + ' р.' : '') + '</strong></p></a></li>').listview('refresh');
					
					try {
						myGeoObjects[i] = new ymaps.GeoObject({
							geometry: {	
								type: "Point",
								coordinates: item['gps']
							},
							properties: {
								clusterCaption: item['name'],
								balloonContentHeader: item['name'],
								balloonContentBody: item['organization'],
								balloonContentFooter: item['title'] + '<br><a href="#" class="my-lw-doctor-results" data-id="' + item['card_id'] + '">Открыть</a>'
							}
						});
					}
					catch (e) {}
				});
				
				try {
					var myClusterer = new ymaps.Clusterer({
						clusterDisableClickZoom: true
					});
					
					myClusterer.add(myGeoObjects);
					doctorMap.geoObjects.add(myClusterer);
				}
				catch (e) {}
				
				$.mobile.changePage('#doctor-results');
			}
			else {
				$('#my-text-error').html('Список врачей, исследований или диагностик не может быть показан!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
		},
		error: function(request) {
			currentAjax = undefined;
			$.mobile.loading('hide');
			
			if (!isSilentAjaxAbort) {
				$('#my-text-error').html('Ошибка доступа к серверу!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
			
			isSilentAjaxAbort = false;
		}
	});	
});

// Действия при загрузке карточки доктора/исследования/диагностики
$(document).on('click', '.my-lw-doctor-results', function() {
	doctorCardId = $(this).data('id');
	$('#my-title-doctor-card').html('Карточка врача');
	
	$('#my-img-doctor-card-logo').attr('src', '').hide();
	$('#my-text-doctor-card-name').html('');
	$('#my-text-doctor-card-title').html('');
	$('#my-text-doctor-card-organization').html('');
	
	$('#my-lw-doctor-card-schedule').html('').listview();
	$('#my-lw-doctor-card-info').html('').listview();
	$('#my-lw-doctor-card-coordinates').html('').listview();
	
	$.mobile.loading('show', {
		text: 'загрузка данных...',
		textVisible: true,
		theme: 'b'
	});
	
	currentAjax = $.ajax({
		type: 'POST',
		url: apiUrl + '?mode=get_doctor_card&callback=?',
		data: 'card_id=' + doctorCardId,
		dataType: 'jsonp',
		timeout: 30000,
		success: function(data) {
			currentAjax = undefined;
			isSilentAjaxAbort = false;
			$.mobile.loading('hide');
			
			if (data['doctor_last_name'] != undefined && data['doctor_last_name'] != '') {
				if (data['logo'] != undefined && data['logo'] != '') {
					$('#my-img-doctor-card-logo').attr('src', data['logo']).show();
				}
			
				$('#my-title-doctor-card').html(data['doctor_last_name'] + ' ' + data['doctor_name'] + ' ' + data['doctor_patr_name']);
				
				$('#my-text-doctor-card-name').html(data['doctor_last_name'] + ' ' + data['doctor_name'] + ' ' + data['doctor_patr_name']);
				$('#my-text-doctor-card-title').html(data['doctor_title']);
				$('#my-text-doctor-card-organization').html(data['view_org'] + ' ' + data['org_name']);
				
				if ((data['category'] != undefined && data['category'] != '') || (data['degree'] != undefined && data['degree'] != '') || (data['price_first'] != undefined && data['price_first'] != '')  || (data['price_second'] != undefined && data['price_second'] != '')) {
					$('#my-lw-doctor-card-info').append('<li data-role="list-divider">Общая информация</li>').listview('refresh');
					
					if (data['category'] != undefined && data['category'] != '') $('#my-lw-doctor-card-info').append('<li style="white-space:normal;"><strong>Категория специалиста:</strong> ' + data['category'] + '</li>').listview('refresh');
					if (data['degree'] != undefined && data['degree'] != '') $('#my-lw-doctor-card-info').append('<li style="white-space:normal;"><strong>Степень:</strong> ' + data['degree'] + '</li>').listview('refresh');
					if (data['price_first'] != undefined && data['price_first'] != '') $('#my-lw-doctor-card-info').append('<li style="white-space:normal;"><strong>Цена первичного приема:</strong> ' + data['price_first'] + ' р.</li>').listview('refresh');
					if (data['price_second'] != undefined && data['price_second'] != '') $('#my-lw-doctor-card-info').append('<li style="white-space:normal;"><strong>Цена повторного приема:</strong> ' + data['price_second'] + ' р.</li>').listview('refresh');
				}
				
				if (data['schedule'] != undefined && data['schedule'] != '') {
					$('#my-lw-doctor-card-schedule').append('<li data-role="list-divider">Расписание приема</li>').listview('refresh');
					$('#my-lw-doctor-card-schedule').append('<li style="white-space:normal;">' + data['schedule'] + '</li>').listview('refresh');
				}
				
				if ((data['address'] != undefined && data['address'] != '') || (data['phones'][0] != undefined && data['phones'][0] != '') || (data['site'] != undefined && data['site'] != '')  || (data['email'] != undefined && data['email'] != '')) {
					$('#my-lw-doctor-card-coordinates').append('<li data-role="list-divider">Координаты</li>').listview('refresh');
					
					if (data['address'] != undefined && data['address'] != '') $('#my-lw-doctor-card-coordinates').append('<li data-icon="location"><a href="https://m.maps.yandex.ru/?text=' + data['address'] + '" style="white-space:normal;" onClick="javascript:return openExternal(this)">' + data['address'] + '</a></li>').listview('refresh');
					if (data['phones'][0] != undefined && data['phones'][0] != '') $('#my-lw-doctor-card-coordinates').append('<li data-icon="phone"><a href="tel://' + data['phones'][0] + '" style="white-space:normal;">' + data['phones'][0] + '</a></li>').listview('refresh');
					if (data['phones'][1] != undefined && data['phones'][1] != '') $('#my-lw-doctor-card-coordinates').append('<li data-icon="phone"><a href="tel://' + data['phones'][1] + '" style="white-space:normal;">' + data['phones'][1] + '</a></li>').listview('refresh');
					if (data['site'] != undefined && data['site'] != '') $('#my-lw-doctor-card-coordinates').append('<li data-icon="info"><a href="' + data['site'] + '" style="white-space:normal;" onClick="javascript:return openExternal(this)">' + data['site'] + '</a></li>').listview('refresh');
					if (data['email'] != undefined && data['email'] != '') $('#my-lw-doctor-card-coordinates').append('<li data-icon="mail"><a href="mailto: ' + data['email'] + '" style="white-space:normal;">' + data['email'] + '</a></li>').listview('refresh');
				}
	
				$.mobile.changePage('#doctor-card');
			}
			else {
				$('#my-text-error').html('Выбранный врач, исследование или диагностика не найдены!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
		},
		error: function(request) {
			currentAjax = undefined;
			$.mobile.loading('hide');
			
			if (!isSilentAjaxAbort) {
				$('#my-text-error').html('Ошибка доступа к серверу!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
			
			isSilentAjaxAbort = false;
		}
	});
});

// Действия при открытии карты доктора/исследования/диагностики
$(document).on('click', '#doctor-results-map-tab', function() {
	if (doctorMap != undefined) {
		doctorMap.container.fitToViewport();
	}
});

// Действия при поиске лекарства или медицинского товара
$(document).on('click', '.my-btn-pharmacy-find', function() {
	pharmacyTerm = ($(this).data('term') != undefined) ? $(this).data('term') : $('#my-text-pharmacy-find').val();
	
	if (pharmacyTerm.trim() == '') {
		$('#my-text-error').html('Заполните текстовое поле для выполнения поиска!');
		$.mobile.changePage('#error', { role: 'dialog' });
		return;
	}
	
	$('#my-lw-pharmacy-results').html('').listview();
	$('#my-text-pharmacy-results').html('Ничего не найдено!');

	$.mobile.loading('show', {
		text: 'загрузка данных...',
		textVisible: true,
		theme: 'b'
	});
	
	currentAjax = $.ajax({
		type: 'POST',
		url: apiUrl + '?mode=get_pharmacy_list&callback=?',
		data: 'term=' + pharmacyTerm,
		dataType: 'jsonp',
		timeout: 30000,
		success: function(data) {
			currentAjax = undefined;
			isSilentAjaxAbort = false;
			$.mobile.loading('hide');
			
			try {
				if (pharmacyMap == undefined) {
					$('#my-pharmacy-map').width(1).height(1);
					
					pharmacyMap = new ymaps.Map('my-pharmacy-map', {
						center: [56.83292145, 60.62570133],
						zoom: 11,
						controls: ['zoomControl', 'typeSelector']
					});
				}				
			
				pharmacyMap.geoObjects.removeAll();
			}
			catch (e) {
				$('#my-pharmacy-map').html('<p style="padding: 10px 0px 0px 10px;">Не удалось загрузить карту! Перезапустите приложение.</p>');
			}
				
			if (data[0] != undefined && data[0]['card_id'] != undefined && data[0]['card_id'] != '') {
				var myGeoObjects = [];
			
				$.each(data, function(i, item) {
					if (i == 0) $('#my-text-pharmacy-results').html('');
					$('#my-lw-pharmacy-results').append('<li><a href="#" class="my-lw-pharmacy-results" data-id="' + item['card_id'] + '"><img ' + ((item['logo'] != undefined && item['logo'] != '') ? 'src="' + item['logo'] + '"' : 'style="display: none;"') + '><h2 style="white-space:normal;">' + item['name'] + '</h2><p style="white-space:normal;"><strong>' + item['organization'] + '</strong></p><p style="white-space:normal;">' + item['address'] + '</p></a></li>').listview('refresh');
					
					try {
						myGeoObjects[i] = new ymaps.GeoObject({
							geometry: {	
								type: "Point",
								coordinates: item['gps']
							},
							properties: {
								clusterCaption: item['name'],
								balloonContentHeader: item['name'],
								balloonContentBody: item['organization'],
								balloonContentFooter: item['address'] + '<br><a href="#" class="my-lw-pharmacy-results" data-id="' + item['card_id'] + '">Открыть</a>'
							}
						});
					}
					catch (e) {}
				});
				
				try {
					var myClusterer = new ymaps.Clusterer({
						clusterDisableClickZoom: true
					});
					
					myClusterer.add(myGeoObjects);
					pharmacyMap.geoObjects.add(myClusterer);
				}
				catch (e) {}
				
				$.mobile.changePage('#pharmacy-results');
			}
			else {
				$('#my-text-error').html('Список лекарств и медицинскох товаров не может быть показан!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
		},
		error: function(request) {
			currentAjax = undefined;
			$.mobile.loading('hide');
			
			if (!isSilentAjaxAbort) {
				$('#my-text-error').html('Ошибка доступа к серверу!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
			
			isSilentAjaxAbort = false;
		}
	});
});

// Действия при загрузке карточки лекарства или медицинского товара
$(document).on('click', '.my-lw-pharmacy-results', function() {
	pharmacyCardId = $(this).data('id');
	$('#my-title-pharmacy-card').html('Карточка лекарства');
	
	$('#my-img-pharmacy-card-logo').attr('src', '').hide();
	$('#my-text-pharmacy-card-name').html('');
	$('#my-text-pharmacy-card-organization').html('');
	
	$('#my-lw-pharmacy-card-price').html('').listview();
	$('#my-lw-pharmacy-card-coordinates').html('').listview();
	$('#my-lw-pharmacy-card-schedule').html('').listview();
	
	$.mobile.loading('show', {
		text: 'загрузка данных...',
		textVisible: true,
		theme: 'b'
	});
	
	currentAjax = $.ajax({
		type: 'POST',
		url: apiUrl + '?mode=get_pharmacy_card&callback=?',
		data: 'card_id=' + pharmacyCardId,
		dataType: 'jsonp',
		timeout: 30000,
		success: function(data) {
			currentAjax = undefined;
			isSilentAjaxAbort = false;
			$.mobile.loading('hide');
			
			if (data['name'] != undefined && data['name'] != '') {
				if (data['logo'] != undefined && data['logo'] != '') {
					$('#my-img-pharmacy-card-logo').attr('src', data['logo']).show();
				}
			
				$('#my-title-pharmacy-card').html(data['name']);
				
				$('#my-text-pharmacy-card-name').html(data['name']);
				$('#my-text-pharmacy-card-organization').html(data['view_org'] + ' ' + data['org_name']);
				
				if (data['cost'] != undefined && data['cost'] != '') {
					$('#my-lw-pharmacy-card-price').append('<li data-role="list-divider">Цена</li>').listview('refresh');
					$('#my-lw-pharmacy-card-price').append('<li style="white-space:normal;">' + data['cost'] + ' р.</li>').listview('refresh');
				}
				
				if ((data['address'] != undefined && data['address'] != '') || (data['phones'][0] != undefined && data['phones'][0] != '') || (data['site'] != undefined && data['site'] != '')  || (data['email'] != undefined && data['email'] != '')) {
					$('#my-lw-pharmacy-card-coordinates').append('<li data-role="list-divider">Координаты</li>').listview('refresh');
					
					if (data['address'] != undefined && data['address'] != '') $('#my-lw-pharmacy-card-coordinates').append('<li data-icon="location"><a href="https://m.maps.yandex.ru/?text=' + data['address'] + '" style="white-space:normal;" onClick="javascript:return openExternal(this)">' + data['address'] + '</a></li>').listview('refresh');
					if (data['phones'][0] != undefined && data['phones'][0] != '') $('#my-lw-pharmacy-card-coordinates').append('<li data-icon="phone"><a href="tel://' + data['phones'][0] + '" style="white-space:normal;">' + data['phones'][0] + '</a></li>').listview('refresh');
					if (data['phones'][1] != undefined && data['phones'][1] != '') $('#my-lw-pharmacy-card-coordinates').append('<li data-icon="phone"><a href="tel://' + data['phones'][1] + '" style="white-space:normal;">' + data['phones'][1] + '</a></li>').listview('refresh');
					if (data['site'] != undefined && data['site'] != '') $('#my-lw-pharmacy-card-coordinates').append('<li data-icon="info"><a href="' + data['site'] + '" style="white-space:normal;" onClick="javascript:return openExternal(this)">' + data['site'] + '</a></li>').listview('refresh');
					if (data['email'] != undefined && data['email'] != '') $('#my-lw-pharmacy-card-coordinates').append('<li data-icon="mail"><a href="mailto: ' + data['email'] + '" style="white-space:normal;">' + data['email'] + '</a></li>').listview('refresh');
				}
				
				if (data['schedule'] != undefined && data['schedule'] != '') {
					$('#my-lw-pharmacy-card-schedule').append('<li data-role="list-divider">Расписание</li>').listview('refresh');
					$('#my-lw-pharmacy-card-schedule').append('<li style="white-space:normal;">' + data['schedule'] + '</li>').listview('refresh');
				}
	
				$.mobile.changePage('#pharmacy-card');
			}
			else {
				$('#my-text-error').html('Выбранное лекарство или медицинский товар не найдены!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
		},
		error: function(request) {
			currentAjax = undefined;
			$.mobile.loading('hide');
			
			if (!isSilentAjaxAbort) {
				$('#my-text-error').html('Ошибка доступа к серверу!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
			
			isSilentAjaxAbort = false;
		}
	});
});

// Действия при открытии карты лекарств/медицинских товаров
$(document).on('click', '#pharmacy-results-map-tab', function() {
	if (pharmacyMap != undefined) {
		pharmacyMap.container.fitToViewport();
	}
});

// Действия при нажатии кнопок на странице вопроса о самочувствии
$(document).on('click', '.my-lw-health-how', function() {
	var id = $(this).data('id');
	var element = $(this).data('element');
	
	if (id == 1) {
		cordova.plugins.notification.local.cancel(element, null);
		
		$.mobile.changePage('#great', { role: 'dialog' });
		
		$('#my-lw-home').show();
		$('#my-text-health-how').html('');
		$('#my-lw-health-how').html('').listview();
	}
	else if (id == 2) {
		isWarning = true;
		navigateDynamicPage(dynamicPageHashMask, element);
		
		$('#my-lw-home').show();
		$('#my-text-health-how').html('');
		$('#my-lw-health-how').html('').listview();
	}
});

//////////////////////////////////////////////////////////////////////////////////////////////

// Действия при перевороте экрана или изменении размера
$(window).on('orientationchange resize', function(event) {
	setActualSize('complaint-show-front', 'complaint-show-rear-tab', 2.62);
	setActualArea('complaint-show-front');
	
	setActualSize('complaint-show-rear', 'complaint-show-rear-tab', 2.62);
	setActualArea('complaint-show-rear');
	
	setActualSize('my-doctor-map', 'doctor-results-map-tab', 0);
	setActualSize('my-pharmacy-map', 'pharmacy-results-map-tab', 0);
});

// Действия при возврате назад
$(window).on('navigate', function(event, data) {
	if (currentAjax != undefined) {
		isSilentAjaxAbort = true;
		currentAjax.abort();
	}
});

//////////////////////////////////////////////////////////////////////////////////////////////

// Функция добавления новой динамической страницы
function navigateDynamicPage(mask, id) {
	var hash = mask + '-' + id;
	
	if ($('#' + hash).length == 0) {
		var page = $('<div data-role="page" class="' + dynamicPageClass + '" id="' + hash + '">' +
			'<div data-role="header" data-theme="b" data-position="fixed" data-tap-toggle="false"><a href="#" data-rel="back" class="ui-btn-left ui-btn ui-icon-back ui-btn-icon-notext ui-shadow ui-corner-all" data-role="button" role="button">Назад</a>' +
			'<a href="#" class="ui-btn-right ui-btn ui-icon-home ui-btn-icon-notext ui-shadow ui-corner-all" data-role="button" role="button">Домой</a><h1>HealBro</h1></div>' + 
			'<div role="main" class="ui-content">' + 
			'<ul data-role="listview" id="my-lw-question-' + id + '" data-inset="true"></li></ul>' +
			'<ul data-role="listview" id="my-lw-answers-' + id + '" data-inset="true"></ul>' + 
			'<ul data-role="listview" id="my-lw-answers-doctors-' + id + '" data-inset="true"></ul>' + 
			'</div></div>');
		page.appendTo($.mobile.pageContainer);
	}
	
	$.mobile.changePage('#' + hash);
}

// Функция добавления ведущего нуля для даты
function addZero(num) {
	return (num < 10) ? '0' + num : num;
}

// Функция открытия ссылки во внешнем браузере
function openExternal(elem) {
	window.open(elem.href, '_system');
	return false;
}

// Функция добавления нестандартного элемента ввода времени
function addTimeWidget(title, idHours, idMinutes, idBtnAdd) {
	var widget = '<fieldset data-role="controlgroup" data-type="horizontal"><legend>' + title + '</legend><select id="' + idHours + '">';
	
	for (var i = 0; i < 24; i++) widget += '<option value="' + addZero(i) + '">' + addZero(i) + '</option>';
	widget += '</select><select id="' + idMinutes + '">';
	
	for (var i = 0; i < 60; i++) {
		if (i % 5 == 0) widget += '<option value="' + addZero(i) + '">' + addZero(i) + '</option>';
	}
	widget += '</select>';
	
	if (idBtnAdd != null && idBtnAdd.length > 0) {
		widget += '<input type="button" id="' + idBtnAdd + '" data-icon="plus" data-iconpos="notext" value="Добавить">';
	}
	
	widget += '</fieldset>';
	
	return widget;
}

// Установка актуального размера блочного элемента
function setActualSize(map, closestNode, scale) {
	if ($('#' + map).offset() != undefined && $('#' + closestNode).offset() != undefined) {
		var x = $('#' + closestNode).offset().left + $('#' + closestNode).outerWidth(true);
		var y = $('#' + closestNode).offset().top + $('#' + closestNode).outerHeight(true);
		
		$('#' + map).width(x);
		
		if (scale > 0) {
			$('#' + map).height(x * scale);
		}
		else {
			$('#' + map).height($(window).height() - y);
		}
	}
}

// Установка актуальных координат горячих областей ссылки
function setActualArea(map) {
	var usemap = $('#' + map).attr('usemap');
	
	if (usemap != undefined) {
		$(usemap + ' > area').each(function(i) {
			var coords = $(this).data('coords');
			
			if (coords != undefined) {
				var arrCoords = coords.replace(/\s/g, '').split(',');
				
				arrCoords = convertDimensions(map, arrCoords);
				$(this).attr('coords', arrCoords.join(','))
			}
		});
	}
}

// Преобразование процентных величин областей ссылки в постоянные
function convertDimensions(map, coords) {
	var ret = undefined;
	
	if ($('#' + map).offset() != undefined) {
		var x = $('#' + map).width();
		var y = $('#' + map).height();
		
		if (coords.length % 2 == 0) {
			for (var i = 0; i < coords.length; i++) {
				if (i % 2 == 0) {
					coords[i] = x * coords[i] / 412;
				}
				else {
					coords[i] = y * coords[i] / 1080;
				}
			}
			
			ret = coords;
		}
	}
	
	return ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////

// Функции обратного вызова уведомлений
document.addEventListener('deviceready', function () {
	cordova.plugins.notification.local.on('click', function (notification) {
		if (notification.id <= 100000) {
			$('#my-lw-home').hide();
			$('#my-text-health-how').html('');
			$('#my-lw-health-how').html('').listview();
			
			if (db) {
				db.transaction(function(tx) {
					tx.executeSql('SELECT healbro_complaints.name, healbro_my_complaints.id_last_node FROM healbro_my_complaints, healbro_complaints WHERE healbro_complaints._id=healbro_my_complaints.id_complaint AND healbro_my_complaints._id=?', [notification.id],
					function (tx, result) {
						if (result.rows.length == 1) {
							$('#my-text-health-how').html('Вы жаловались на следующие симптомы: <b>' + result.rows.item(0)['name'] + '</b>.<br>Как самочувствие на данный момент?');
							
							$('#my-lw-health-how').append('<li data-icon="check"><a href="#" class="my-lw-health-how" data-id="1" data-element="' + notification.id + '">Отлично!</a></li>').listview('refresh');
							$('#my-lw-health-how').append('<li data-icon="alert"><a href="#" class="my-lw-health-how" data-id="2" data-element="' + result.rows.item(0)['id_last_node'] + '">Жалоба повторяется</a></li>').listview('refresh');
						}
					});
				});
			}
			else {
				$('#my-text-error').html('Ошибка доступа к локальной базе данных!');
				$.mobile.changePage('#error', { role: 'dialog' });
			}
		}
	});
}, false);